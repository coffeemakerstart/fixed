<?php

namespace App\Providers;

use App\Repositories\BranchRepository;

use App\Repositories\CompanyRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\BranchAddressRepository;
use App\Repositories\BranchRepositoryInterface;
use App\Repositories\CompanyRepositoryInterface;
use App\Repositories\BranchAddressRepositoryInterface;
use App\Repositories\ItemRepository;
use App\Repositories\ItemRepositoryInterface;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CompanyRepositoryInterface::class, CompanyRepository::class);
        $this->app->bind(BranchRepositoryInterface::class, BranchRepository::class);
        $this->app->bind(BranchAddressRepositoryInterface::class, BranchAddressRepository::class);
        $this->app->bind(ItemRepositoryInterface::class, ItemRepository::class);
    }
}
