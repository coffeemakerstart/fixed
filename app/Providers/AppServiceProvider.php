<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // exported the passport migrations and set to ignoreMigrations to the default
        \Laravel\Passport\Passport::ignoreMigrations();

        // run telescope in local and staging (production temporarily)
        if (\in_array($this->app->environment(), ['local', 'staging', 'prod'])) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // add code here
    }
}
