<?php

namespace App\Repositories;

use App\Entities\Item;
use App\Entities\BranchTypeItem;
use App\Entities\BranchItem;

class ItemRepository implements ItemRepositoryInterface
{
    /**
     * Gets items by branch
     *
     * @param int $branchId
     * @return array
     */
    public function findByBranch(int $branchId)
    {
        $items = BranchItem::where('branch_id', '=', $branchId)->with(['item']);
        return $items;
    }

    /**
     * Gets items by shop type
     *
     * @param int $shopTypeId
     * @return array
     */
    public function findByShopType(int $shopTypeId)
    {
        $items = BranchTypeItem::where('shop_type_id', '=', $shopTypeId)->with(['item']);
        return $items;
    }

    /**
     * Update availability of branch items
     *
     * @param int $branchItemId
     * @param bool $availability
     * @return BranchItem
     */
    public function updateItemAvailability(int $branchItemId, bool $availability)
    {
        $item = BranchItem::find($branchItemId);

        $itemData = [
            'last_updated_at' => date("Y-m-d H:i:s"),
            'availability' => $availability
        ];
        
        $item->update($itemData);

        $item->save();
        $item->touch();

        return $item;
    }
}
