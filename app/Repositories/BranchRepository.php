<?php

namespace App\Repositories;

use App\Entities\Branch;
use App\Entities\BranchItem;
use Illuminate\Support\Carbon;
use App\Entities\BranchAddress;
use App\Repositories\ItemRepository;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\BranchRepositoryInterface;
use App\Http\Resources\Branch\BranchTypeItem as BranchTypeItemResource;

class BranchRepository implements BranchRepositoryInterface
{

    /**
     * Gets branches by filters
     *
     * @param array $filters
     * @return
     */
    public function find(array $filters, array $sort = [])
    {
        return Branch::all();
    }

    /**
     * Find by company Id
     *
     * @param integer $branchId
     * @param bool $withTrashed
     * @return Branch
     */
    public function findById(int $branchId, bool $withTrashed = true): Branch
    {
        if($withTrashed) {
            return Branch::withTrashed()->find($branchId);
        } 
            
        return Branch::find($branchId);
        
    }

    /**
     * @inheritDoc
     */
    public function create(array $branchData, BranchAddress $address = null)
    {
        $branch = new Branch();
        $branch->name = $branchData['name'];
        $branch->owner_company_id = $branchData['company_id'];
        $branch->owner_user_id = $branchData['owner_user_id'];
        $branch->email = $branchData['email'];
        $branch->website = $branchData['website'];

        if (isset($branchData['has_queue'])) {
            $branch->has_queue = $branchData['has_queue'];
        }

        if (isset($branchData['average_waiting_time'])) {
            $branch->average_waiting_time = $branchData['average_waiting_time'];
        }

        if (isset($branchData['tp_number'])) {
            $branch->tp_number = $branchData['tp_number'];
        }

        if (isset($branchData['google_place_id'])) {
            $branch->google_place_id = $branchData['google_place_id'];
        }

        if (isset($branchData['shop_type'])) {
            $branch->shop_type = $branchData['shop_type'];
        }

        $branch->save();

        if ($address) {
            $branch->address()->save($address);
        }

        $this->assignItems($branch->id, $branchData['shop_type']);
        
        return Branch::find($branch->id);
    }

    /**
     * @todoThis should be moved to a new reposirtory 
     * Assign essential items to branch
     */
    private function assignItems($branchId, $shopType)
    {
        // Asign items to branch
        $itemRepostory = new ItemRepository();

        $items = BranchTypeItemResource::collection(
            $itemRepostory->findByShopType($shopType)->get()
        );

        $branchItems = [];

        foreach ($items as $item) {
            array_push($branchItems, [
                'branch_id' => $branchId,
                'item_id' => $item['item_id'],
                'created_at' => Carbon::now(),
                'availability' => true
            ]);
        }

        BranchItem::insert($branchItems);
    }

    /**
     * @inheritDoc
     */
    public function update(int $branchId, array $branchData)
    {

        $branch = $this->findById($branchId);
        $branch->update($branchData);

        $branch->save();
        $branch->touch(); //updates timestamps

        return $branch;
    }

    /**
     * @inheritDoc
     */
    public function delete(int $branchId)
    {

        $branch = $this->findById($branchId);
        $branch->delete();

        $branch->save();
        
        return $branch;
    }

    /**
     * @inheritDoc
     */
    public function findByGooglePlaceId(string $googlePlaceId)
    {
        $branch = Branch::where('google_place_id', '=', $googlePlaceId)
            ->firstOrFail();

        return $branch;
    }

    /**
     * Find by a name similar to the given
     *
     * @param string $branchName
     * @return void
     */
    public function getBySimilarName(string $branchName) {
        return Branch::where('name', 'like', $branchName . '%');
    }

    /**
     * Get by geo location
     *
     * @param [type] $latitude
     * @param [type] $longitude
     * @param integer $radius
     * @return void
     */
    public function getByLocation($latitude, $longitude, $radius = 1000)
    {
        /*
         * works for now may be spatial way would be faster
         * replace 6371000 with 6371 for kilometer and 3956 for miles
         */

        $branches = BranchAddress::selectRaw("*")
            ->whereRaw("(6371000 * acos( cos( radians(?) ) *
               cos( radians( latitude ) )
               * cos( radians( longitude ) - radians(?)
               ) + sin( radians(?) ) *
               sin( radians( latitude ) ) )
            ) < ?", [$latitude, $longitude, $latitude, $radius])
            ->with('branch');

        return $branches;
    }
}
