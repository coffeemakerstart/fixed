<?php

namespace App\Repositories;

use App\Entities\Branch;
use App\Entities\BranchAddress;
use App\Repositories\BranchAddressRepositoryInterface;

class BranchAddressRepository implements BranchAddressRepositoryInterface
{

    /**
     * Gets branch address by filters
     *
     * @param array $filters
     * @return
     */
    public function find(array $filters, array $sort = [])
    {
        $branchAdresses = BranchAddress::orderBy('locality');
        if (\in_array('city', $filters)) {
            $branchAdresses->where('locality', 'LIKE', '%' . $filters['city'] . '%');
        }

        return $branchAdresses;
    }

    /**
     * Find by branchAddress By BranchId
     *
     * @param integer $branchId
     * @param bool $withTrashed
     * @return BranchAdrress
     */
    public function findByBranchId(int $branchId, bool $withTrashed = true): BranchAddress
    {
        if($withTrashed) {
            return BranchAddress::withTrashed()->firstWhere('branch_id', '=', $branchId);
        }
        return BranchAddress::firstWhere('branch_id', '=', $branchId);
    }

    /**
     * Find by branchAddress Id
     *
     * @param integer $branchId
     * @param bool $withTrashed
     * @return BranchAdrress
     */
    public function findById(int $branchAddressId, bool $withTrashed = true): BranchAdrress
    {
        if($withTrashed) {
            return BranchAddress::find($branchAddressId);
        }
        return BranchAddress::find($branchAddressId);
    }

    /**
     * search addresses which has city|locality like
     *
     * @param integer $branchId
     * @return
     */
    public function getByCity(string $city)
    {
        return BranchAddress::where('locality', 'like', '%' . $city . '%')
            ->orderBy('locality')
            ->with('branch');
    }

    /**
     * search by lat long with radius
     *
     * @param float $latitude
     * @param float $longitude
     * @param integer $radius
     * @return
     */
    public function getByGeoLocation(float $latitude, float $longitude, int $radius = 10)
    {
        return BranchAddress::all(); // to be implemented
    }

    /**
     * @inheritDoc
     */
    public function create(array $addressData, bool $save = false)
    {
        $branchAddress = new BranchAddress();
        // keeping this full address given it might be useful due to locality issues at the state due to weird sl addresses
        if (isset($addressData['address'])) {
            $branchAddress->address = $addressData['address'];
        }

        $branchAddress->street = $addressData['street'];
        // possible to have no building number in SL
        if (isset($addressData['building_number'])) {
            $branchAddress->building_number = $addressData['building_number']; // building no | house no
        }
        $branchAddress->locality = $addressData['locality']; // city | town
        $branchAddress->state = $addressData['state']; // state | province
        $branchAddress->country_code = $addressData['country_code']; // country code
        if (isset($addressData['postal_code'])) {
            $branchAddress->postal_code = $addressData['postal_code']; // building no | house no
        }

        $branchAddress->latitude = $addressData['latitude'];
        $branchAddress->longitude = $addressData['longitude'];

        if ($save) {
            // this part is not required when saving through a relationship
            $branchAddress->branch_id = $addressData['branch_id'];
            $branchAddress->save();
        }

        return $branchAddress;
    }

    /**
     * @inheritDoc
     */
    public function update(int $branchId, array $addressData)
    {

        $branchAddress = $this->findByBranchId($branchId);
        $branchAddress->update($addressData);

        $branchAddress->save();
        $branchAddress->touch();
        return $branchAddress;
    }

    /**
     * Delete for BranchAddress
     *
     * @param BranchAddress $branchAddress
     * @return BranchAddress
     */
    public function delete(BranchAddress $branchAddress) 
    {
        $branchAddress->delete();
        $branchAddress->save();
        
        return $branchAddress;
    }

    // public function findByLocation($latitude, $longitude, $radius = 1)
    // {
    //     /*
    //      * works for now may be spatial way would be faster
    //      * replace 6371000 with 6371 for kilometer and 3956 for miles
    //      */
    //     $addresses = BranchAddress::select("*",  DB::raw('( 6371000 * acos( cos( radians(?) ) *
    //     cos( radians( latitude ) )
    //     * cos( radians( longitude ) - radians(?)
    //     ) + sin( radians(?) ) *
    //     sin( radians( latitude ) ) )
    //   ) AS distance'),
    //         [$latitude, $longitude, $latitude]
    //     )
    //         ->having("distance", "<=", $radius)
    //         ->orderBy("distance", 'asc');

    //     return $addresses;
    // }

    //id, branch_id, address, latitude, longitude, street, building_number,
    //postal_code, locality, state country_code, deleted_at, created_at, updated_at
}
