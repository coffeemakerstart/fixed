<?php

namespace App\Repositories;

/**
 * This interface is not ideal but works for now
 * Company Repository interface
 */
interface CompanyRepositoryInterface
{
    /**
     * find method
     *
     * @param array $filters
     * @param array $sort
     * @return <Company> 
     */
    public function find(array $filters, array $sort);


    /**
     * Create method
     *
     * @param array $companyData
     * @return Company
     */
    public function create(array $companyData);


    /**
     * company update
     *
     * @param int $company
     * @param array $companyData
     * @return Company
     */
    public function update(int $companyId, array $companyData);

    /**
     * Find company by ID
     *
     * @param integer $companyId
     * @return Company
     */
    public function findById(int $companyId);

}
