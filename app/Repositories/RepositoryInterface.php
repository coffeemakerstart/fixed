<?php

namespace App\Repositories;

interface RepositoryInterface
{
    /**
     * find method
     *
     * @param array $filters
     * @param array $sort
     * @return void
     */
    public function find(array $filters, array $sort);

}
