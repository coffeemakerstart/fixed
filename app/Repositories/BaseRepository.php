<?php

namespace App\Repositories;

use App\Entities\Company;
use Illuminate\Database\Eloquent\Model;

class BaseRepository
{

    protected Model $baseModel;

    public function __construct(Model $model) {
        $this->baseModel =  $model;
    }

    /**
     * Get the baseModel
     */ 
    public function getBaseModel()
    {
        return $this->baseModel;
    }

    public function find(array $filters, array $sort = []) {
    
    }
}