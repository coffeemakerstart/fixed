<?php

namespace App\Repositories;

use App\Entities\BranchAddress;

/**
 * BranchAddress Repository interface
 */
interface BranchAddressRepositoryInterface
{
    /**
     * find method
     *
     * @param array $filters
     * @param array $sort
     * @return <BranchAddress> 
     */
    public function find(array $filters, array $sort);

    /**
     * Get By city
     *
     * @param string $city
     * @return <Branch>
     */
    public function getByCity(string $city);

    /**
     * search by lat long with radius
     *
     * @param float $latitude
     * @param float $longitude
     * @param integer $radius
     * @return
     */
    public function getByGeoLocation(float $latitude, float $longitude, int $radius = 10);

    /**
     * Create address
     *
     * @param array $addressData
     * @param boolean $save
     * @return BranchAddress
     */
    public function create(array $addressData, bool $save);


    /**
     * branch address update
     *
     * @param int $branchId
     * @param array $addressData
     * @return BranchAddress
     */
    public function update(int $branchId, array $addressData);

    /**
     * Find branch address by ID
     *
     * @param integer $branchId
     * @param bool $withTrashed
     * @return BranchAddress
     */
    public function findByBranchId(int $branchId);

     /**
     * Find by branchAddress Id
     *
     * @param integer $branchId
     * @param bool $withTrashed
     * @return BranchAdrress
     */
    public function findById(int $branchAddressId, bool $withTrashed = true);

    
    public function delete(BranchAddress $branchAddress);

}
