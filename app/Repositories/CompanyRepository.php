<?php

namespace App\Repositories;

use App\Entities\Company;

class CompanyRepository implements CompanyRepositoryInterface
{

    /**
     * Gets companies by filters
     *
     * @param array $filters
     * @return
     */
    public function find(array $filters, array $sort = [])
    {

        $companies = Company::orderBy('name');

        if (isset($filters['name'])) {
            $companies->where('name', 'like', '%' . $filters['name'] . '%');
        }
        if (isset($filters['reg_number'])) {
            $companies->where('reg_number', '=', $filters['reg_number']);
        }
        if (isset($filters['email'])) {
            $companies->where('email', '=', $filters['email']);
        }

        /**
         * @todo list
         * need to think of a global filtering model for all requests since the above is a bit ugly
         * implement sort
         */
        return $companies;
    }

    /**
     * Find by company Id
     *
     * @param integer $companyId
     * @return
     */
    public function findById(int $companyId)
    {
        return Company::find($companyId);
    }

    /**
     * @inheritDoc
     */
    public function create(array $companyData)
    {
        $company = new Company();
        $company->name = $companyData['name'];
        $company->reg_number = $companyData['reg_number'];
        $company->email = $companyData['email'];
        $company->country_code = $companyData['country_code'];
        $company->tp_number = $companyData['tp_number'];

        $company->save();

        return $company;
    }

    /**
     * @inheritDoc
     */
    public function update(int $companyId, array $companyData)
    {

        $company = $this->findById($companyId);
        $company->update($companyData);

        $company->save();

        return $company;
    }

}
