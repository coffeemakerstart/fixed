<?php

namespace App\Repositories;

use App\Entities\BranchAddress;

/**
 * 
 * Branch Repository interface
 */
interface BranchRepositoryInterface
{
    /**
     * find method
     *
     * @param array $filters
     * @param array $sort
     * @return <Company> 
     */
    public function find(array $filters, array $sort);


    /**
     * Create Method
     *
     * @param array $branchData
     * @param BranchAddress $address
     * @return void
     */
    public function create(array $branchData, BranchAddress $address);


    /**
     * branch update
     *
     * @param int $branchId
     * @param array $branchData
     * @return Branch
     */
    public function update(int $branchId, array $branchData);

    /**
     * branch delete
     *
     * @param int $branchId
     * @return Branch
     */
    public function delete(int $branchId);

    /**
     * Find branch by ID
     *
     * @param integer $branchId
     * @param bool $withTrashed
     * @return Branch
     */
    public function findById(int $branchId, bool $withTrashed = true);

    /**
     * Get branch by google places id
     *
     * @param string $googlePlaceId
     * @return void
     */
    public function findByGooglePlaceId(string $googlePlaceId);

   /**
    * get by geo loaction
    *
    * @param [type] $latitude
    * @param [type] $longitude
    * @param integer $radius
    * @return void
    */
    public function getByLocation($latitude, $longitude, $radius = 1000);
}
