<?php

namespace App\Repositories;

/**
 * Item Repository interface
 */
interface ItemRepositoryInterface
{
    public function findByBranch(int $branchId);

    public function findByShopType(int $shopTypeId);

    public function updateItemAvailability(int $branchItemId, bool $availability);
}
