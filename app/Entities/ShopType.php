<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ShopType extends Model
{
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_types';
}
