<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $guarded = ['owner_user_id'];
    
    /**
     * Get the owner of a company.
     */
    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_user_id', 'id')->withDefault();
    }

    /**
     * Get the owned branches.
     */
    public function ownedBranches()
    {
        return $this->hasMany('App\Entities\Branch', 'owner_company_id', 'id');
    }

}
