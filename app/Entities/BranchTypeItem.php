<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BranchTypeItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'branch_type_items';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    protected $fillable = ['shop_type_id'];

    /**
     * Get the items for the branch type.
     */
    public function item()
    {
        return $this->belongsTo('App\Entities\Item');
    }
}
