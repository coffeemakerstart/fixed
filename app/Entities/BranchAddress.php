<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchAddress extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branch_address';

    protected $fillable = [
        'address',
        'street',
        'building_number',
        'locality',
        'state',
        'postal_code',
        'country_code',
        'latitude',
        'longitude',
    ];
    
    /**
     * Get the branch.
     */
    public function branch()
    {
        return $this->belongsTo('App\Entities\Branch', 'branch_id', 'id')->withDefault();
    }

    /**
     * Untestable quick and easy method definetly need spatial
     *
     * @param [type] $query
     * @param [type] $latitude
     * @param [type] $longitude
     * @param integer $radius
     * @return void
     */
    public static function scopeIsWithinDistance($query, $latitude, $longitude, $radius = 2) {

        $haversine = "(6371 * acos(cos(radians(?)) 
                        * cos(radians(`latitude`)) 
                        * cos(radians(`longitude`) 
                        - radians(?)) 
                        + sin(radians(?)) 
                        * sin(radians(`latitude`))))";
    
        return $query->select('*')
                     ->selectRaw(
                         "{$haversine} AS distance", 
                        [$latitude, $longitude, $latitude]
                     )
                     ->whereRaw("{$haversine} < ?", [$radius]);
    }
}
