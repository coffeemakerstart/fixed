<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BranchItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'branch_items';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    protected $fillable = ['branch_id', 'item_id', 'last_updated_at', 'availability'];

    protected $casts = [
        'last_updated_at' => 'timestamp',
        'availability' => 'bool'
    ];

    /**
     * Get the items for the branch type.
     */
    public function item()
    {
        return $this->belongsTo('App\Entities\Item');
    }
}
