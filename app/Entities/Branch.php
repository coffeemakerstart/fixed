<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;
    
    const CROWD_STATUS_LOW = 'LOW';
    const CROWD_STATUS_MODERATE = 'MODERATE';
    const CROWD_STATUS_HIGH = 'HIGH';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_branches';

    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $guarded = ['owner_user_id'];

    /**
     * Get the owner of a company.
     */
    public function ownerCompany()
    {
        return $this->belongsTo('App\Entities\Company', 'owner_company_id', 'id')->withDefault();
    }

    /**
     * Get the owner user
     */
    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_user_id', 'id')->withDefault();
    }

    /**
     * Get the owned branches.
     */
    public function address()
    {
        return $this->hasone('App\Entities\BranchAddress', 'branch_id', 'id');
    }

    public function isBusy()
    {
        return $this->crowd !== self::CROWD_STATUS_HIGH;
    }

    public function isOpen()
    {
        return $this->is_open;
    }

    public static function getCrowdStatuses()
    {
        return [
            self::CROWD_STATUS_LOW,
            self::CROWD_STATUS_MODERATE,
            self::CROWD_STATUS_HIGH,
        ];
    }
}
