<?php

namespace App\Http\Resources\Branch;

use App\Http\Resources\Branch\BranchAddress as BranchAddressResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Branch extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request   
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'has_queue' => $this->has_queue,
            'average_waiting_time' => $this->average_waiting_time,
            'is_open' => $this->is_open,
            'crowd' => $this->crowd,
            'email' => $this->email,
            'website' => $this->website,
            'owner_company_id' => $this->owner_company_id,
            'rating' => $this->rating,
            'tp_number' => $this->tp_number,
            'shop_type' => $this->shop_type,
            'queue_updated_at' => $this->queue_updated_at,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'location' => new BranchAddressResource($this->whenLoaded('address'))
        ];
    }
}
