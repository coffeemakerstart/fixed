<?php

namespace App\Http\Resources\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchFromAddress extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->branch->id,
            'name' => $this->branch->name,
            'has_queue' => $this->branch->has_queue,
            'average_waiting_time' => $this->average_waiting_time,
            'is_open' => $this->branch->is_open,
            'crowd' => $this->branch->crowd,
            'email' => $this->branch->email,
            'website' => $this->branch->website,
            'owner_company_id' => $this->branch->owner_company_id,
            'country_code' => $this->branch->country_code,
            'rating' => $this->branch->rating,
            'tp_number' => $this->branch->tp_number,
            'queue_updated_at' => $this->branch->queue_updated_at,
            'deleted_at' => $this->branch->deleted_at,
            'created_at' => $this->branch->created_at,
            'updated_at' => $this->branch->updated_at,
            'location' => [
                'street' => $this->street,
                'building_number' => $this->building_number,
                'locality' => $this->locality,
                'state' => $this->state,
                'postal_code' => $this->postal_code,
                'country_code' => $this->country_code,
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
            ],
        ];
    }
}
