<?php

namespace App\Http\Middleware;

use App\Repositories\BranchRepository;
use Closure;
use Illuminate\Support\Facades\Auth;

class checkBranchOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $branchRepository = new BranchRepository();
        $branchId = $request->route('branch');
        $branch = $branchRepository->findByid($branchId);
        if ($branch) {
            if (Auth::user()->id !== $branch->owner_user_id) {
                // @todo:: throw an exception and manage the responce via exceptions
                return response()->json([
                    'message' => 'The given branch is not created by you',
                    'errors' => [
                        'branch' => 'The given branch is not created by you'
                    ]
                ], 403);
            }
        }

        return $next($request);
    }
}
