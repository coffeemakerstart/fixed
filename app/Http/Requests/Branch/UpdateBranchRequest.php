<?php

namespace App\Http\Requests\Branch;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $branchId = $this->route('branch');
        return [
            'name' => 'required|string|max:150',
            'website' => 'string|url',
            'email' => 'string|email', // |unique:company_branches,' . $branchId note: unique might not be ideal for developing coutries
            'tp_number' => 'string|phone:AUTO',
            'has_queue' => 'required|boolean',
            'location.street' => 'required|string',
            'location.building_number' => 'string',
            'location.locality' => 'required|string',
            'location.state' => 'required|string',
            'location.latitude' => 'required|numeric|between:-90,90',
            'location.longitude' => 'required|numeric|between:-180,180',
            // 'location.country_code' => 'required|string', // @todo :: Something verify when country code table is added
        ];
    }
}
