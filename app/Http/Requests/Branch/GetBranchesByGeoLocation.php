<?php

namespace App\Http\Requests\Branch;

use Illuminate\Foundation\Http\FormRequest;

class GetBranchesByGeoLocation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latitude' => 'required|between:-90,90',
            'longitude' => 'required|between:-180,180',
            'radius' => 'numeric'
        ];
    }

    /**
     * Undocumented function
     *
     * @param [type] $keys
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['latitude'] = $this->input('latitude');
        $data['longitude'] = $this->input('longitude');
        $data['radius'] = $this->input('radius');

        return $data;
    }
}
