<?php

namespace App\Http\Requests\Branch;

use App\Repositories\BranchRepository;
use Illuminate\Foundation\Http\FormRequest;

class CreateBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:150',
            'company_id' => 'required|exists:companies,id',
            'website' => 'string|url',
            'email' => 'string|email', // note: unique might not be ideal for developing coutries
            'tp_number' => 'string|phone:AUTO',
            'has_queue' => 'required|boolean',
            'shop_type' => 'exists:shop_types,id',
            'location.street' => 'required|string',
            'location.building_number' => 'string',
            'location.locality' => 'required|string',
            'location.state' => 'required|string',
            'location.latitude' => 'required|numeric|between:-90,90',
            'location.longitude' => 'required|numeric|between:-180,180',
            // 'location.country_code' => 'required|string', // @todo :: Something verify when country code table is added
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->isDuplicate($this->all())) {
                $validator->errors()->add('name', 'Branch|Store seems like a duplicate, please check name and location again!');
            }
        });
    }

    /**
     * Checks if its a possible duplicate
     *
     * @param array $requestData
     * @return void
     */
    public function isDuplicate(array $requestData)
    {
        // @todo :: load repo via constructor using interface
        $branchRepostory = new BranchRepository();
        $similarBranches = $branchRepostory->getBySimilarName($requestData['name'])->get();

        $givenlocation = $requestData['location'];
        foreach ($similarBranches as $branch) {
            if ($branch->address->latitude === floatval($givenlocation['latitude'])
                && $branch->address->longitude === floatval($givenlocation['longitude'])
            ) {
                return true;
            }

        }

        return false;
    }
}
