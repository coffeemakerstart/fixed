<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Branch\CreateBranchRequest;
use App\Http\Requests\Branch\UpdateBranchRequest;
use App\Http\Requests\Branch\UpdateCrowdStatusRequest;
use App\Http\Requests\Branch\UpdateQueueStatusRequest;
use App\Http\Resources\Branch\Branch as BranchResource;
use App\Http\Resources\Branch\BranchFromAddress as BranchFromAddressResource;
use App\Repositories\BranchAddressRepositoryInterface;
use App\Repositories\BranchRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class BranchController extends Controller
{
    /**
     * @var App\Repositories\BranchRepository $branchRepostory
     */
    protected $branchRepostory;

    /**
     * @var App\Repositories\BranchAddressRepository $branchAddressRepostory
     */
    protected $branchAddressRepostory;

    private $pageSize;

    public function __construct(
        BranchRepositoryInterface $branchRepostory,
        BranchAddressRepositoryInterface $branchAddressRepostory
    ) {
        $this->branchRepostory = $branchRepostory;
        $this->branchAddressRepostory = $branchAddressRepostory;

        $this->pageSize = Config::get('pagination.page_size', '10');
    }

    /**
     * Get all stores
     *
     * @return JSONResponse
     */
    public function index(Request $request)
    {
        // Limit to 200 branches
        $filters = $request->only(['city']);

        $branches = BranchFromAddressResource::collection(
            $this->branchAddressRepostory->getByCity($filters['city'])->paginate(200)
        );

        return $branches;
    }

    /**
     * Get all stores by GeoLocation
     *
     * @return JSONResponse
     */
    public function getByGeoLocation(Request $request)
    {
        // Limit to 200 branches
        // radius is in meters
        $filters = $request->only(['latitude', 'longitude', 'radius']);

        $radius = 2000; // 2000 meters
        if ($request->has('radius')) {
            $radius = $filters['radius'];
        }

        $branches = BranchFromAddressResource::collection(
            $this->branchRepostory->getByLocation($filters['latitude'], $filters['longitude'], $filters['radius'])->paginate(200)
        );

        return $branches;
    }

    /**
     * Gets a specific branch item
     *
     * @param integer $company
     * @return JSONResponse
     */
    public function show(int $branch, Request $request)
    {
        return new BranchResource($this->branchRepostory->findByid($branch));
    }

    /**
     * Creates new brach
     *
     * @return JSONResponse
     */
    public function create(CreateBranchRequest $request)
    {

        $addressData = $request->only(['location'])['location'];
        $addressData['country_code'] = 'LK'; // hard code to LK to be removed at one point

        $branchData = $request->only(
            [
                'name', 'website', 'email', 'country_code',
                'tp_number', 'company_id', 'has_queue', 'shop_type',
            ]
        );
        if(empty($branchData['shop_type'])) {
            $branchData['shop_type'] = 1; // setting to default @todo please remove this in the next release
        }
        
        $branchData['owner_user_id'] = Auth::user()->id; // add user id

        $branchAddress = $this->branchAddressRepostory->create($addressData);

        $branch = $this->branchRepostory->create($branchData, $branchAddress);

        $branch->load(['address']); // loading address this for the resource
        return new BranchResource($branch);
    }

    /**
     * Update Branch
     *
     * @param integer $branchId
     * @return JSONResponse
     */
    public function update(UpdateBranchRequest $request, int $branchId)
    {

        $branchData = $request->only(['name', 'website', 'email', 'tp_number', 'company_id', 'has_queue']);
        $addressData = $request->only(['location'])['location'];

        //@todo handle try catch or use exception handler for all
        $branch = $this->branchRepostory->update($branchId, $branchData);
        $branchAddress = $this->branchAddressRepostory->update($branchId, $addressData);

        $branch->load(['address']); // loading address this for the resource
        // lets return the updated object back instead of a empty 200
        return new BranchResource($branch);
    }

    /**
     * Update queue status
     *
     * @param Request $request
     * @param integer $branchId
     * @return void
     */
    public function updateQueueStatus(UpdateQueueStatusRequest $request, int $branchId)
    {

        $queueData['average_waiting_time'] = $request->average_waiting_time;
        $branch = $this->branchRepostory->update($branchId, $queueData);
        // @todo:: need a log here in future of who updated

        return $branch->isClean('average_waiting_time');

    }

    /**
     * Update Crowd status
     *
     * @param Request $request
     * @param integer $branchId
     * @return void
     */
    public function updateCrowdStatus(UpdateCrowdStatusRequest $request, int $branchId)
    {

        // request validation

        $busyStatus['crowd'] = $request->crowd;
        $this->branchRepostory->update($branchId, $busyStatus);
        // @todo:: need a log here in future of who updated
    }

    /**
     * Delete Branch
     *
     * @param integer $branchId
     * @return JSONResponse
     */
    public function destroy(int $branchId)
    {   
        $branch = $this->branchRepostory->delete($branchId);

        $branchAddress = $this->branchAddressRepostory->findByBranchId($branchId);
        $branchAddress = $this->branchAddressRepostory->delete($branchAddress);

        return $branch->trashed() && $branchAddress->trashed();
    }
}
