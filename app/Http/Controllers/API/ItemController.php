<?php

namespace App\Http\Controllers\API;

use App\Entities\item;
use App\Http\Controllers\Controller;
use App\Http\Requests\Branch\UpdateItemAvailabilityRequest;
use App\Http\Resources\Item as ItemResource;
use App\Http\Resources\Branch\BranchItem as BranchItemResource;
use App\Http\Resources\Branch\BranchTypeItem as BranchTypeItemResource;
use App\Repositories\ItemRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ItemController extends Controller
{
    /**
     * @var App\Repositories\ItemRepositoryInterface $itemRepostory
     */
    protected $itemRepostory;

    public function __construct(ItemRepositoryInterface $itemRepostory)
    {
        $this->itemRepostory = $itemRepostory;
    }

    /**
     * Get all items per branch
     *
     * @return JSONResponse
     */
    public function getBranchItems(int $branch, Request $request)
    {
        $pageSize = Config::get('pagination.page_size', '10');

        $items = BranchItemResource::collection(
            $this->itemRepostory->findByBranch($branch)->paginate($pageSize)
        );

        return $items;
    }

    /**
     * Get all items per shop type
     *
     * @return JSONResponse
     */
    public function getShopItems(int $shopType, Request $request)
    {
        $pageSize = Config::get('pagination.page_size', '10');

        $items = BranchTypeItemResource::collection(
            $this->itemRepostory->findByShopType($shopType)->paginate($pageSize)
        );

        return $items;
    }

    /**
     * Update item availability
     *
     * @return JSONResponse
     */
    public function updateAvailability(UpdateItemAvailabilityRequest $request, int $branch)
    {
        $itemData = $request->only(['id', 'availability']);
        $branchItemId = $itemData['id'];
        $availability = $itemData['availability'];

        return new BranchItemResource($this->itemRepostory->updateItemAvailability($branchItemId, $availability));
    }
}
