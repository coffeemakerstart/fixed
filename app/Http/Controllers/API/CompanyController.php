<?php

namespace App\Http\Controllers\API;

use App\Entities\Company;
use App\Http\Controllers\Controller;
use App\Http\Resources\Company as CompanyResource;
use App\Repositories\CompanyRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class CompanyController extends Controller
{
    /**
     * @var App\Repositories\CompanyRepository $companyRepostory
     */
    protected $companyRepostory;

    public function __construct(CompanyRepositoryInterface $companyRepostory)
    {
        $this->companyRepostory = $companyRepostory;
    }

    /**
     * Get all companies
     *
     * @return JSONResponse
     */
    public function index(Request $request)
    {
        $pageSize = Config::get('pagination.page_size', '10');

        $companies = CompanyResource::collection(
            $this->companyRepostory->find($request->all())->paginate($pageSize)
        );

        return $companies;
    }

    /**
     * Gets a specific company item
     *
     * @param integer $company
     * @return JSONResponse
     */
    public function show(int $company, Request $request)
    {
        return new CompanyResource($this->companyRepostory->findByid($company));
    }

    /**
     * Creates new company
     *
     * @return JSONResponse
     */
    public function create(Request $request)
    {
        // @todo discussion on rules to be moved to the model
        $request->validate([
            'name' => 'required|string|max:150',
            'reg_number' => 'string|max:50',
            'email' => 'string|email',// note: unique might not be ideal for developing coutries
            'country_code' => 'required|string|max:2', //@todo :: Something verify when country code table is added
            'tp_number' => 'string|phone:AUTO',
        ]);
        $companyData = request(['name', 'reg_number', 'email', 'country_code', 'tp_number']);

        // lets return the created object back
        return new CompanyResource($this->companyRepostory->create($companyData));
    }

    /**
     * Update company
     *
     * @param integer $companyId
     * @return JSONResponse
     */
    public function update(Request $request, int $company)
    {

        $request->validate([
            'name' => 'required|string|max:150',
            'reg_number' => 'string|max:50',
            'email' => 'string|email',
            'country_code' => 'required|string|max:2',
            'tp_number' => 'string|phone:AUTO',
        ]);
        $companyData = request(['name', 'reg_number', 'email', 'tp_number', 'country_code']);

        // lets return the updated object back instead of a empty 200
        return new CompanyResource($this->companyRepostory->update($company, $companyData));
    }
}
