<?php

namespace App\Console\Commands;

use App\Entities\Company;
use Illuminate\Console\Command;

class CleanFakeCompanies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:delete {company}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete company and related branches by company id ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $companyId = $this->argument('company');
        $company = Company::withTrashed()->find($companyId);   
        
        foreach($company->ownedBranches as $branch) {
            $branch->address()->delete(); // soft delete address of a branch
        }

        $company->ownedBranches()->delete(); //soft delete all branches
        $company->delete();

        echo 'All Branches related to the company {' .  $companyId . '} were successfully deleted';

        return 1;
    }
}
