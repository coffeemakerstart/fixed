# SHOP with CONFIDENCE

## Issue

During this crisis I found it difficult to find out all needed shopping items in one shop where I had to go for different shops for complete full shopping list. Also, I had to wait at the ques for so long as all my neighbours decided to shop at the same time. I found that most of my friends also face this challenge where it took us long time to complete the shopping while giving more probability on disclosing to the virous.


## Solution

We are creating a mobile application where community will update the availability of the grocery when they shop. So, the community get an understand of the stocks and go the exact shop where their needs are fulfilling. 

We hope to take the solution to the next level by implementing an appointment system for the shops, where you can shop at your booked time slot.


## Benefits

* Avoid long queue leading better social distance.

* Spends less time out of home can help to less interference.

* Older people can easily book a time for the shopping without waiting in a que.


