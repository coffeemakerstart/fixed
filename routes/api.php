<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'API\AuthController@login')->name('login');
    Route::post('register', 'API\AuthController@register');
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'API\AuthController@logout');
        Route::get('user', 'API\AuthController@user');
    });
});

Route::group([
    'prefix' => 'v1' //versioning
], function () {
    Route::group([
        'prefix' => 'companies',
        'middleware' => ['api', 'auth:api']
    ], function () {
        Route::get('', 'API\CompanyController@index');
        Route::get('/{company}', 'API\CompanyController@show');
        Route::post('/', 'API\CompanyController@create');
        Route::put('/{company}', 'API\CompanyController@update');
    });

    Route::group([
        'prefix' => 'branches',
        'middleware' => 'auth:api'
    ], function () {
        Route::get('', 'API\BranchController@index');
        Route::get('/get-by-geo', 'API\BranchController@getByGeoLocation');
        Route::get('/{branch}', 'API\BranchController@show');
        Route::post('/', 'API\BranchController@create');
        Route::put('/{branch}', 'API\BranchController@update')->middleware('branch.owner');
        Route::delete('/{branch}', 'API\BranchController@destroy')->middleware('branch.owner');
        Route::patch('/{branch}/update-queue', 'API\BranchController@updateQueueStatus');
        Route::get('/{branch}/items', 'API\ItemController@getBranchItems');
        Route::put('/{branch}/items/update-availability', 'API\ItemController@updateAvailability');
    });

    Route::group([
        'prefix' => 'items',
        'middleware' => 'auth:api'
    ], function () {
        Route::get('/{shopType}', 'API\ItemController@getShopItems');
    });
});
