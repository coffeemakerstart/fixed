<?php

use Illuminate\Database\Seeder;
use App\Entities\BranchItem;

class BranchItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BranchItem::truncate();

        $date = date("Y-m-d H:i:s");

        $branch_items = [
            ['branch_id' => 1, 'item_id' => 1, 'last_updated_at' => $date, 'availability' => true],
            ['branch_id' => 1, 'item_id' => 2, 'last_updated_at' => $date, 'availability' => true],
            ['branch_id' => 1, 'item_id' => 3, 'last_updated_at' => $date, 'availability' => false],
            ['branch_id' => 1, 'item_id' => 4, 'last_updated_at' => $date, 'availability' => true],
            ['branch_id' => 1, 'item_id' => 5, 'last_updated_at' => $date, 'availability' => true],
            ['branch_id' => 1, 'item_id' => 6, 'last_updated_at' => $date, 'availability' => true],
            ['branch_id' => 2, 'item_id' => 7, 'last_updated_at' => $date, 'availability' => true]
        ];

        foreach($branch_items as $item) {
            BranchItem::create($item);
        }
    }
}
