<?php

use App\Entities\ShopType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShopType::truncate();

        $shopTypes = [
            ['name' => 'supermarket', 'has_items' => true, 'average_waiting_time' => 0],
            ['name' => 'pharmacy', 'has_items' => true, 'average_waiting_time' => 2],
            ['name' => 'dentist', 'has_items' => false, 'average_waiting_time' => 30],
            ['name' => 'doctor', 'has_items' => false, 'average_waiting_time' => 25],
            ['name' => 'fruit', 'has_items' => true, 'average_waiting_time' => 0],
            ['name' => 'salon', 'has_items' => false, 'average_waiting_time' => 20],
            ['name' => 'hardware', 'has_items' => true, 'average_waiting_time' => 1]
        ];

        foreach($shopTypes as $shopType) {
            ShopType::create(
                $shopType
            );
        }
        
    }
}
