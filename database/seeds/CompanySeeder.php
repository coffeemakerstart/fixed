<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Entities\Company::truncate(); // truncate companies
        App\Entities\Branch::truncate(); // truncate companies
        App\Entities\BranchAddress::truncate(); // truncate branch addresses

        // $companies = factory(App\Entities\Company::class, 10)->create(); // creates 10 records

        // Create 10 records of companies with single branches
        factory(App\Entities\Company::class, 10)->create()->each(function ($company) {

            $company->save();
            $branch = $company->ownedBranches('delinquent')->save(
                factory(App\Entities\Branch::class)->make(['owner_company_id' => $company->id])
            );

            $branch->address()->save(
                factory(App\Entities\BranchAddress::class)->make(['branch_id' => $branch->id])
            );
        });

        // Create 5 records of companies with multiple branches
        factory(App\Entities\Company::class, 5)->create()->each(function ($company) {

            $company->save();
            $branches = $company->ownedBranches('delinquent')->saveMany(
                factory(App\Entities\Branch::class, 2)->make(['owner_company_id' => $company->id])
            );

            foreach($branches as $branch) {
                $branch->address()->save(
                    factory(App\Entities\BranchAddress::class)->make(['branch_id' => $branch->id])
                );
            }
            
        });

        /**
         * @todo:: assert database changes and
         * 1) create user owned companies
         * 2) add users to the companies to the users
         */
    }
}
