<?php

use Illuminate\Database\Seeder;
use App\Entities\BranchTypeItem;

class BranchTypeItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BranchTypeItem::truncate();

        $branch_type_items = [
            ['shop_type_id' => 1, 'item_id' => 1],
            ['shop_type_id' => 1, 'item_id' => 2],
            ['shop_type_id' => 1, 'item_id' => 3],
            ['shop_type_id' => 1, 'item_id' => 4],
            ['shop_type_id' => 1, 'item_id' => 5],
            ['shop_type_id' => 1, 'item_id' => 6],
            ['shop_type_id' => 2, 'item_id' => 7],
            ['shop_type_id' => 5, 'item_id' => 5],
            ['shop_type_id' => 5, 'item_id' => 6],
            ['shop_type_id' => 7, 'item_id' => 8]
        ];

        foreach($branch_type_items as $item) {
            BranchTypeItem::create($item);
        }
    }
}
