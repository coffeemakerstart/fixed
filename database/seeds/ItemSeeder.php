<?php

use App\Entities\Item;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::truncate();

        $items = [
            ['id' => 1, 'name' => 'Biscuit'],
            ['id' => 2, 'name' => 'Rice'],
            ['id' => 3, 'name' => 'Chicken'],
            ['id' => 4, 'name' => 'Fish'],
            ['id' => 5, 'name' => 'Apple'],
            ['id' => 6, 'name' => 'Grapes'],
            ['id' => 7, 'name' => 'Panadol'],
            ['id' => 8, 'name' => 'Cement']
        ];

        foreach($items as $item) {
            Item::create($item);
        }
    }
}
