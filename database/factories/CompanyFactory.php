<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Company;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Company::class, function (Faker $faker) {

    $faker->addProvider(new \Faker\Provider\en_US\Company($faker));
    return [
        'name' => $faker->company  . ' ' . $faker->companySuffix,
        'email' => $faker->unique()->companyEmail,
        'tp_number' => $faker->e164PhoneNumber,
        'reg_number' => $faker->ein 
    ];
});
