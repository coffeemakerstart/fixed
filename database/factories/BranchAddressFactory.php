<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Entities\BranchAddress;

$factory->define(BranchAddress::class, function (Faker $faker) {
    return [
        'address' => $faker->address,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'street' => $faker->streetName,
        'building_number' => $faker->buildingNumber,
        'postal_code' => $faker->postcode,
        'locality' => $faker->city,
        'state' => $faker->state,
        'country_code' => $faker->countryCode,
    ];
});
