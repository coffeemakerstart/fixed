<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Entities\Branch;
use Faker\Generator as Faker;

$factory->define(Branch::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\en_US\Company($faker));
    return [
        'name' => $faker->company  . ' branch ' . $faker->randomNumber(3),
        'rating' => $faker->numberBetween(1, 5),
        'email' => $faker->companyEmail,
        'tp_number' => $faker->e164PhoneNumber,
        'website' => $faker->url,
    ];
});
