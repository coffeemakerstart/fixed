<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // use and index for lat long , name, not sure we need for address
        Schema::create('company_branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('owner_company_id');
            $table->integer('owner_user_id')->nullable();
            $table->decimal('rating', 3, 2)->default(0);
            $table->integer('user_ratings_total')->default(0);
            $table->boolean('has_queue')->default(true);
            $table->boolean('is_open')->default(true); // update in the scheduler in future
            $table->integer('average_waiting_time')->default(0); // in minutes
            $table->timestampTz('queue_updated_at', 0);
            $table->string('crowd', 10)->default('LOW'); // LOW | MODERATE | HIGH
            $table->string('tp_country_code', 2)->nullable(); // research needed to keep without +
            $table->string('tp_number', 25)->nullable(); // lets assume we will not allow multiple numbers here for now
            $table->string('website', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('google_place_id', 255)->nullable();
            $table->softDeletesTz('deleted_at', 0);
            $table->timestampsTz();
        });

        Schema::create('branch_address', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('branch_id');
            $table->text('address')->nullable(); // could exceed 255
            $table->double('latitude', 18, 15)->nullable();
            $table->double('longitude', 18, 15)->nullable();
            // if this works well lat long can be removed of the location filled https://github.com/grimzy/laravel-mysql-spatial
            // $table->point('location')->default('POINT(0, 0)');
            $table->string('street');
            $table->string('building_number')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('locality'); // city
            $table->string('state')->nullable(); // province
            $table->string('country_code', 2)->nullable();
            $table->softDeletesTz('deleted_at', 0);
            $table->timestampsTz();

            // will comment this for now for future reference
            // if (Config::get('database.default') != 'sqlite') {
            //     //spatial indexes are not in sqlite for CI
            //     $table->spatialIndex('location');

            // }

            $table->index('latitude');
            $table->index('longitude');
            $table->index('locality'); // another search item

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_branches');
        Schema::dropIfExists('branch_address');
    }
}
