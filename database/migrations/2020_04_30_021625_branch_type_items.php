<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BranchTypeItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_type_items', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->integer('shop_type_id')->unsigned();
            $table->unsignedBigInteger('item_id');
            $table->timestamps();

            // $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            // $table->foreign('shop_type_id')->references('id')->on('shop_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branch_type_items');
    }
}
