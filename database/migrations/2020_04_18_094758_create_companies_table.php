<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            // note: skipping uuids due to possible managements issues
            $table->increments('id');
            $table->string('name', 255);
            $table->string('reg_number', 255)->nullable();
            $table->string('email', 255)->unique()->nullable();
            $table->integer('owner_user_id')->nullable();
            $table->decimal('rating', 3, 2)->default(0);
            $table->string('country_code', 2)->nullable(); // since tp country might not match residing country 
            $table->string('tp_country_code', 2)->nullable(); // research needed to keep without + 
            $table->string('tp_number', 25)->nullable(); // lets assume we will not allow multiple numbers here for now
            // sattellite number might come into equation
            $table->timestampTz('verified_at', 0);
            $table->softDeletesTz('deleted_at', 0);
            $table->timestampsTz();

            // leave foreign keys for now 
            //$table->foreign('owner_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
